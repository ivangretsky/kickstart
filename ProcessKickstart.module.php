<?php namespace ProcessWire;
class ProcessKickstart extends Process {

	private $recipePath; // folder where the recipies are stored
	private $recipe = null; // current recipe filename
	private $recipes = []; // array of all available recipes
	private $kick; // instance of the kickstarter

	/**
	 * prepare initial variables of this module
	 */
	public function init() {
		parent::init();

		// get the kickstarter instance
		define('bootstrap', true);
		require_once(__DIR__ . '/kickstart.php');
		$this->kick = new Kickstart($this->wire);

		// prepare all recipes
		$this->recipePath = $this->config->paths->root . 'site/assets/' . $this->className;
		$this->recipe = @$this->input->post->recipe; // store the post value as current recipe

		// loop all files
		if(is_dir($this->recipePath)) {
			$dir = new \DirectoryIterator($this->recipePath);
			foreach ($dir as $fileinfo) {
				if($fileinfo->isDot()) continue;
				$filename = $fileinfo->getFilename();
				$attr = null;

				// if the post value is set we assign the selected attribute
				if( $this->recipe AND $this->recipe == $filename) $attr = ['selected'=>'selected'];

				// add this to the recipes array
				$this->recipes[] = [$filename, $attr];
			}

			// if the recipe was not set via POST we take the first file
			if(!$this->recipe AND count($this->recipes)) $this->recipe = $this->recipes[0][0];
		}
	}

	/**
	 * main process module screen
	 */
	public function ___execute() {
		$post = $this->input->post;
		$filepath = $this->recipePath .'/'. $this->recipe;
		$recipes = [];
		$form = $this->modules->get('InputfieldForm');

		// process input before rendering the form
			// save code
				if($post->save OR $post->run) {
					file_put_contents($filepath, $post->codeeditor);
				}
			// run code
			if($post->run) {
				$this->kick->runPHP($filepath);
			}


		// select recipe
			$f = $this->modules->get('InputfieldSelect');
			$f->label = $this->_('Select Recipe');
			$f->attr('id+name', 'recipe');
			$f->required = 1;
			if(@$post->recipe) $f->collapsed = Inputfield::collapsedYes;
			foreach($this->recipes as $recipe) $f->addOption($recipe[0], null, $recipe[1]);

			// show note if we find no recipes
			if(!count($this->recipes)) {
				$f->notes = $this->_("No Recipes found, place them here:") . " {$this->recipePath}";
			}
			else {
				$f->notes = $this->_("To add new recipes place them here:") . " {$this->recipePath}";
			}

			$form->add($f);

		// code editor
			$f = $this->modules->get('InputfieldAceExtended');
			$f->attr('id+name', 'codeeditor');
			$f->label = $this->_('Code Editor');
			$f->theme = 'monokai';
			$f->mode = 'php';
			$f->value = file_get_contents($filepath);
			$f->notes = $this->_('Loaded file:') . " $filepath";
			$form->add($f);

		// output
			$f = $this->modules->get('InputfieldMarkup');
			$f->label = $this->_('Output');
			$f->value = $this->kick->renderMessages();
			$form->add($f);

		// buttons
			$f = $this->modules->get('InputfieldSubmit');
			$f->attr('id+name', 'run');
			$f->value = $this->_('Save + Run (CTRL+ENTER)');
			$form->add($f);

			$f = $this->modules->get('InputfieldSubmit');
			$f->attr('id+name', 'save');
			$f->value = $this->_('Save');
			$f->addClass('ui-priority-secondary');
			$form->add($f);
	
		return $form->render() . $this->kick->branding();
	}

	/**
	 * things to do on module install
	 */
	public function ___install() {
		parent::___install();

		// move sample recipe to site assets
		$dir = $this->config->paths->assets . 'ProcessKickstart';
		if(!is_dir($dir)) mkdir($dir);
		copy(__DIR__ . '/assets/sample-recipe.php', $dir . '/sample-recipe.php');
		
    // track this installation
    $http = new WireHttp();
    $http->post('http://www.google-analytics.com/collect', [
      'v' => 1, // Version
      'tid' => 'UA-76905506-1', // Tracking ID / Property ID.
      'cid' => 555, // Anonymous Client ID.
      't' => 'event', // hit type
      'ec' => 'PWModules', // category
      'ea' => 'install', // action
      'el' => $this->className, // label
    ]);
	}
	
  public function uninstall() {
		parent::___uninstall();
		
    // track this uninstallation
    $http = new WireHttp();
    $http->post('http://www.google-analytics.com/collect', [
      'v' => 1, // Version
      'tid' => 'UA-76905506-1', // Tracking ID / Property ID.
      'cid' => 555, // Anonymous Client ID.
      't' => 'event', // hit type
      'ec' => 'PWModules', // category
      'ea' => 'uninstall', // action
      'el' => $this->className, // label
    ]);
  }
	
}

