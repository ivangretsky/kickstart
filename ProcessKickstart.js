$(document).ready(function() {
	// submit form on field change
	$('#recipe').change(function() {
		$(this)
			.closest('form') // find the form
			.find('#run').attr('name',null) // find the submit button and remove the name not to execute the run action
			.click().click(); // click the button
	});

	// submit form on ctrl+enter
	$('#wrap_codeeditor').keydown(function (e) {
		if (e.ctrlKey && e.keyCode == 13) {
			$('#run').click();
		}
	});

	// ace editor tweaks
	$("#codeeditor").on('loaded', function() {
		var editor = $(this).getAce();
		editor.setOptions({
			maxLines: Infinity,
			tabSize: 2,
		});
		editor.focus(); //To focus the ace editor
		var n = editor.getSession().getValue().split("\n").length; // To count total no. of lines
		editor.gotoLine(n); // go to end of document
		editor.execCommand("gotolineend"); // go to end of line
	});
}); 
