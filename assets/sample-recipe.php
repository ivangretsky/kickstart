<?php namespace ProcessWire;
/**
 * example recipe for pw kickstart
 * 
 * @author Bernhard Baumrock, baumrock.com
 * @version 1
 * 
 * ------------
 * Available variables in this file:
 * 
 * $this              current kickstart instance (see section "recipe helper functions" in the file Kickstart.php)
 * $wire              the processwire instance
 * $config            the processwire $config api variable
 * $pages             the processwire $pages api variable
 * $recipesettings    recipesettings set in the kickstartfile
 * 
 * In the ProcessModule the recipes lie in /site/templates/assets
 * In the regular installer the recipes lie in /recipes
 * The site root folder is available as $this->siteRoot
 */

$this->msg('Running this sample recipe...');

$this->msg("Value of \$recipesettings['foo']: ".@$recipesettings['foo']);

// download and install a module
// first parameter has to be the module's classname
// CAUTION: the modules list in pw backend shows the titles of the modules, not the classname (eg "Cache" should be "FieldtypeCache")
$this->installModule('FieldtypeRepeater');

// you can also download directly from any web adress and set module settings on success
// use $this->wire->modules->getConfig($yourmodule) to see all options
if($tracy = $this->installModule('TracyDebugger', 'https://github.com/adrianbj/TracyDebugger/archive/master.zip')) {
  $this->wire->modules->saveConfig($tracy, [
    'superuserForceDevelopment' => 1,
  ]);
}

// install the kickstart process module (recipe editor)
$this->installModule('InputfieldAceExtended', 'https://github.com/owzim/pw-inputfield-ace-extended/archive/master.zip');
$this->installModule('ProcessKickstart', 'https://gitlab.com/baumrock/kickstart/repository/master/archive.zip');

// download and extract to a custom directory (eg template skeleton)
// todo

$this->succ('Sample Recipe was executed successfully!');
